const express = require('express')
const session = require('express-session')
const bodyparser = require('body-parser')
const path = require('path')
const atms = express();
const ejs = require('ejs')
atms.set('views', path.join(__dirname, 'views'));
atms.set('view engine', 'ejs');
atms.use(bodyparser.json());
atms.use(bodyparser.urlencoded({ extended: true }));
atms.use(express.static(path.resolve(__dirname + '/')))

atms.get('/',(req,res)=>{
    res.render('index')
})

atms.get('/home',(req,res)=>{
    res.render('welcome')
})

atms.get('/add-user',(req,res)=>{
    res.render('adduser')
})

atms.listen(process.env.PORT || 3000)
console.log('server is listening at port 3000');